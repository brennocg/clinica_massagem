# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class Meeting(models.Model):
    """ Model for Calendar Event

        Special context keys :
            - `no_mail_to_attendees` : disabled sending email to attendees when creating/editing a meeting
    """

    _inherit = 'calendar.event'

    order_id = fields.Many2one('sale.order', string='Order Reference', ondelete='cascade', index=True,
                               copy=False)
    sala_id = fields.Many2one('sale.salas', string='Sala')
    employee_id = fields.Many2one('hr.employee', string='Terapeuta')
    dt_atendendo = fields.Datetime(string="Data e Hora do Início do Atendimento")
    dt_conluido = fields.Datetime(string="Data e Hora da Conclusão do Atendimento")


class CalendarMudaStatusA(models.TransientModel):
    _name = "calendar.mudastatusa"
    _description = "Muda Status do Agendamento"

    @api.model
    def _get_ids(self):
        calendarevent_obj = self.env['calendar.event']
        event = calendarevent_obj.browse(self._context.get('active_ids'))[0]
        return 0

    selecao_status = fields.Selection([
        ('atendendo', 'Atendendo'),
        ('concluido', 'Concluído')
    ], string='Escolhe o Status da Venda: ', default=_get_ids, required=True)
    dt_atendendo = fields.Datetime(string="Data e Hora do Início do Atendimento")
    dt_conluido = fields.Datetime(string="Data e Hora da Conclusão do Atendimento")

    @api.onchange('selecao_status')
    def _dt_atendimento(self):
        if self.selecao_status == 'atendendo':
            self.dt_atendendo = fields.Datetime.now()
            self.dt_conluido = None
        if self.selecao_status == 'concluido':
            self.dt_conluido = fields.Datetime.now()
            self.dt_atendendo = None

    @api.multi
    def muda_status(self):
        calendar_event = self.env['calendar.event'].browse(self._context.get('active_ids', []))
        for line in calendar_event:
            line.progress = self.selecao_status

            if self.dt_atendendo != None:
                line.dt_atendendo = self.dt_atendendo
            elif self.dt_conluido != None:
                line.dt_conluido = self.dt_conluido

