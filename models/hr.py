# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Employee(models.Model):

    _inherit = 'hr.employee'
    
    # Informacao do Massagista
    massagista = fields.Boolean('É Massagista')
    nivel_massagista = fields.Selection([
        ('n_3', 'Não Formado (3º Semestre)'),
        ('n_4', 'Não Formado (4º Semestre)'),
        ('formado', 'Formado')
    ], string='Nível do Massagista')
    product_ids = fields.Many2many('product.template', 'massagista_produto_rel', 'mid', 'product_id', string='Produtos', domain=[('pacote_ok','!=','False'),('voucher_ok','!=','False'),('valepresente_ok','!=','False')])
    percentual_segsab = fields.Float('Percentual segunda a sabado', digits=(2, 2), default=0.0)
    percentual_domfer = fields.Float('Percentual domingo e feriado', digits=(2, 2), default=0.0)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    