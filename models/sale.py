# -*- coding: utf-8 -*-

from lxml import etree
from datetime import datetime, timedelta
from pytz import timezone
import pytz
import time
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from boto.ec2containerservice.exceptions import ClientException


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    
    sala_id = fields.Many2one('sale.salas', related='order_line.sala_id', string='Sala')
    massagista_id = fields.Many2one('sale.fila', related='order_line.massagista_id', string='Terapeuta')
    massagista_id_quebra = fields.Many2one('sale.fila', string='Terapeuta da Quebra')
    massagista_id_passagem = fields.Many2one('sale.fila', string='Terapeuta da Passagem')
    perc_comissao = fields.Float('Percentual Terapeuta', digits=(6, 2), default=0.0)
    perc_comissao_qbr = fields.Float('Percentual Terapeuta Quebra', digits=(6, 2), default=0.0)
    valor_massagista = fields.Float('Valor Terapeuta', digits=(6, 2), default=0.0)
    valor_massagista_qbr = fields.Float('Valor Terapeuta Quebra', digits=(6, 2), default=0.0)
    vr_saldo_pacote = fields.Monetary(string='Total Saldo', readonly=True)
    tipo_produto = fields.Selection([
        ("comum_pacote", "Padrão"),
        ("pacote", "Adquirir um Pacote"),
        ("item_pac", "Usar um Item de Pacote"),
        ("cortesia", "Cortesia"),
        ("add_vp", "Adquirir Vale Presente"),
        #("add_lv", "Adquirir Lote Voucher"),
        ("usa_vp", "Usar Vale Presente"),
        #("usa_vo", "Usar Voucher")
        ], "Tipo do Produto", default="comum_pacote")
    cortesia_prod = fields.Many2one('sale.cortesia', string='Cortesia')
    valevoucher_prod = fields.Many2one('sale.valevoucher', string='Vale Voucher')
    vr_inicial = fields.Float('Número:', digits=(9, 0), default=0.0)
    vr_final = fields.Float('Número do Último Vale ou Voucher:', digits=(9, 0), default=0.0)
    preferencial = fields.Selection([
        ('n','Sem Preferência'),
        ('pt', 'Preferência Terapeuta'),
        ('ph', 'Preferência Homem'),
        ('pm', 'Preferência Mulher'),
        ('po', 'Preferência Oriental'),
        ('poh', 'Preferência Oriental Homem'),
        ('pom', 'Preferência Oriental Mulher')], "Preferêncial", related='order_line.preferencial', default='n')
    quebra_massagem = fields.Boolean(string='Quebrar Massagem')
    passa_massagem = fields.Boolean(string='Passar Massagem')
    product_id_quebra = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict')
    gestante = fields.Boolean(string='É Gestante')
    progress = fields.Selection([
            ('agendado', 'Agendado'),
            ('atendendo', 'Atendendo'),
            ('concluido', 'Concluído')
        ], string='Status do Atendimento',
        default='agendado',
        help="Status do Atendimento")
    pontuado = fields.Selection([
        ("nao", "Não"),
        ("sim", "Sim")
        ], "Foi Pontuado", default="nao")
    date_end = fields.Datetime(string="Data e Hora do Fim")
    sale_order_line_usopacote_ids = fields.One2many(
        comodel_name='sale.usopacotes',
        inverse_name='order_id',
        string='Uso do Pacote',
        copy=False
    )
    order_usopacote = fields.One2many('sale.usopacotes', 'order_id', string='Uso dos Pacotes',
                                 copy=True,
                                 auto_join=True)
    order_calendar = fields.One2many('calendar.event', 'order_id', string='Calendar Event',
                                    copy=True,
                                    auto_join=True
                                    )

    @api.onchange('tipo_produto')
    @api.multi
    def _onchange_tipo_produto(self):
        for order in self:
            if order.order_id.tipo_produto == "pacote" or order.order_id.tipo_produto == "add_vp":
                return {'domain': {'produto_id': [('pacote_ok', '=', True)]}}
            elif order.order_id.tipo_produto == "pacote" or order.order_id.tipo_produto == "add_vp":
                return {'domain': {'produto_id': [('pacote_ok', '=', False)]}}

    @api.onchange('partner_id')
    def pacote_cortesia_change(self):
        sale_pacote = self.env['sale.pacote']
        self.total_package = 0
    
        where_query = sale_pacote._where_calc([
            ('partner_id', '=', self.partner_id.id), ('saldo', '<>', '0'), ('company_id', '=', self.env.user.company_id.id)]) 
        sale_pacote._apply_ir_rules(where_query, 'read')
        from_clause, where_clause, where_clause_params = where_query.get_sql()
                
        # get saldo
        query = """
            SELECT SUM(saldo) as saldo
                FROM sale_pacote
                WHERE %s
                GROUP BY partner_id
                """ % where_clause
                
        self.env.cr.execute(query, where_clause_params)
        price_totals = self.env.cr.dictfetchall()
        self.total_package = sum(price['saldo'] for price in price_totals)
        self.vr_saldo_pacote = self.total_package

    @api.onchange('date_order')
    def date_order_change(self):
        for order in self:
            order.date_end = order.date_order

    @api.model
    def create(self, values):
        # Override the original create function for the res.partner model
        record = super(SaleOrder, self).create(values)
        record.pacote_cortesia_change()
        values['vr_saldo_pacote'] = record.total_package

        return record
            

class SaleSalas(models.Model):
    _name = 'sale.salas'
    _description = 'Cadastro de Salas e Cadeiras'
    _inherit = ['mail.thread']
    
    # Cadastro de Salas
    sala_name = fields.Char(string='Descrição')
    numero_sala = fields.Char(string='Número da Sala/Cadeira')
    andar = fields.Char(string='Andar da Sala/Cadeira')
    tipo = fields.Selection([
        ('sala', 'Sala'),
        ('cadeira', 'Cadeira')
    ], string='Tipo', default='sala', readonly=False)
    status = fields.Selection([
        ('livre', 'Livre'),
        ('ocupada', 'Ocupada'),
        ('manutencao', 'Em Manutenção')
    ], string='Status', default='livre', readonly=False)
    image = fields.Binary("Imagem", attachment=True,
        help="This field holds the image used as avatar for this room, limited to 1024x1024px",)
    image_small = fields.Binary("Small-sized image", attachment=True)
    
    @api.multi
    def name_get(self):
        result = []
        for sala in self:
            result.append((sala.id, _("%(tipo_sc)s %(sala_name)s, Nº %(sala_num)s, %(andar_sc)s Andar") % {
                'tipo_sc': sala.tipo,
                'sala_name': sala.sala_name,
                'sala_num': sala.numero_sala,
                'andar_sc': sala.andar
            }))
        return result

    
class SaleFila(models.Model):
    _name = 'sale.fila'
    _inherit = 'hr.attendance'
    _order = "kanban_state, pontuacao, check_in"
    
        
    pontuacao = fields.Float('Pontuação', digits=(6, 2))
    vr_atraso_ch = fields.Float('Atraso Chegada', digits=(6, 2), default=0.0)
    vr_atraso_ad = fields.Float('Atraso Adicional', digits=(6, 2), default=0.0)
    kanban_state = fields.Selection([
            (1, 'Na Vez'),
            (2, 'Livre'),
            (3, 'Atendendo'),
            (4, 'Ausente'),
            (5, 'Freelancer'),
            (6, 'Fora da Fila'),
            (7, 'Inativo'),
            (8, 'Arquivado')
        ], string='Status',
        default=1)
    vr_turno = fields.Float('Valor do Turno', digits=(6, 2), default=0.0)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    ultima_zerada = fields.Datetime(string="Última Vez Que Zerou")
    preferencia = fields.Char(string='Preferência')
        
    
    def name_get(self):
        result = []
        if self.env.context.get('special_display_name'):
            for record in self:
                result.append((record.id, _("%(name)s, Status: %(status)s, %(pontos)s pts") % {
                    'name': record.employee_id.name_related,
                    'status': dict(record._fields['kanban_state'].selection).get(record.kanban_state),
                    'pontos': record.pontuacao
                }))
        else:
            # Do a for and set here the standard display name, for example if the standard display name were name, you should do the next for
            for record in self:
                result.append((record.id, _("%(name)s ") % {
                    'name': record.employee_id.name_related
                }))
            
        return result
    
    
class SaleCortesia(models.Model):
    _name = 'sale.cortesia'         
    _description = 'Cortesia'
    
    partner_id = fields.Many2one('res.partner', string='Cliente')
    produto = fields.Many2one('product.product', string='Produto', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict', required=True)
    validade = fields.Date('Validade')
    status = fields.Selection([
            ('ativo', 'Ativo'),
            ('inativo', 'Inativo')
        ], string='Kanban State',
        default='ativo',
        required=True,
        help="")
    dt_uso = fields.Datetime('Data de Uso')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    
    @api.multi
    def name_get(self):
        result = []
        for cortesia in self:
            result.append((cortesia.id, _("%(partner_name)s / %(prod_name)s ") % {
                'partner_name': cortesia.partner_id.name,
                'prod_name': cortesia.produto.product_tmpl_id.name,
            }))
        return result
    
    
class SalePacote(models.Model):
    _name = 'sale.pacote'         
    _description = 'Pacote'
    
    def _default_company(self):
        return self.env['res.company']._company_default_get('res.partner')
    
    
    partner_id = fields.Many2one('res.partner', string='Cliente')
    produto = fields.Many2one('product.product', string='Produto', domain=[('sale_ok', '=', True), ('pacote_ok', '=', True)], change_default=True, ondelete='restrict', required=True)
    validade = fields.Date('Validade')
    vr_pacote = fields.Float('Valor do Pacote', digits=(6, 2), default=0.0)
    vr_usado = fields.Float('Valor Usado', digits=(6, 2), default=0.0)
    saldo = fields.Float('Saldo', digits=(6, 2), default=0.0)
    company_id = fields.Many2one('res.company', 'Company', index=True, default=_default_company)
    order_id = fields.Many2one('sale.order', string='Venda')

    @api.multi
    def name_get(self):
        result = []
        for pacote in self:
            result.append((pacote.id, _("%(prod_name)s de %(partner_name)s ") % {
                'prod_name': pacote.produto.product_tmpl_id.name,
                'partner_name': pacote.partner_id.name,
            }))
        return result
    

class SaleCombo(models.Model):
    _name = 'sale.combo'         
    _description = 'Combo'
    
    product_id_combo = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True), ('combo_ok', '=', True)], change_default=True, ondelete='restrict', required=True)
    product_id_compoe = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True), ('pacote_ok', '=', False), ('combo_ok', '=', False)], change_default=True, ondelete='restrict', required=True)
    vr_prod_pac = fields.Float('Saldo', digits=(6, 2), default=0.0)
    
    @api.multi
    def name_get(self):
        result = []
        for combo in self:
            result.append((combo.id, _("%(prod_combo)s (R$ %(valor)s)") % {
                'prod_combo': combo.product_id_combo.product_tmpl_id.name,
                'valor': combo.vr_prod_pac,
            }))
        return result
    
    
class SaleGestante(models.Model):
    _name = 'sale.gestante'         
    _description = 'Gestantes'
    
    sale_order_id = fields.Many2one('sale.order', string='Venda Referente', required=True, ondelete='restrict', index=True, copy=False)
    image = fields.Binary("Image do Atestado", attachment=True)
    
    @api.multi
    def name_get(self):
        result = []
        for gestante in self:
            result.append((gestante.id, _("%(sale)s") % {
                'sale': gestante.sale_order_id.name,
            }))
        return result
    
  
class SaleValeVoucher(models.Model):
    _name = 'sale.valevoucher'         
    _description = 'ValeVoucher'
    
    
    tipo = fields.Selection([
            ('vp', 'Vale Presente'),
            ('voucher', 'Voucher')
        ], string='Tipo')
    numero = fields.Integer('Numeração')
    partner_id = fields.Many2one('res.partner', string='Customer', required=True, change_default=True, index=True)
    product_id = fields.Many2one('product.product', string='Produto Voucher', domain=[('sale_ok', '=', True), ('voucher_ok', '=', True)], change_default=True, ondelete='restrict', required=True)
    vr_pago = fields.Float('Valor do Voucher', digits=(6, 2), default=0.0) 
    partner_id_uso = fields.Many2one('res.partner', string='Customer', change_default=True, index=True)
    kanban_state = fields.Selection([
            ('ativo', 'Ativo'),
            ('usado', 'Usado'),
            ('bloqueado', 'Bloqueado')
        ], string='Status do Voucher',
        default='inativo')
    note = fields.Text('Motivo')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    
    @api.multi
    def name_get(self):
        result = []
        for vp_vo in self:
            result.append((vp_vo.id, _("%(numero)s %(cliente)s") % {
                'numero': vp_vo.numero,
                'cliente': vp_vo.partner_id.name
            }))
        return result

        
class SaleZeraFila(models.TransientModel):
    _name = "sale.zerafila"
    _description = "Zera Fila"
    
    @api.model
    def _get_ids(self):
        salefila_obj = self.env['sale.fila']
        order = salefila_obj.browse(self._context.get('active_ids'))[0]
        return 0

    selecao_zera = fields.Selection([
        (0, 'Zerar Apenas Pontuação'),
        (1, 'Zerar Apenas Atrasos'),
        (2, 'Zerar Pontuação e Atrasos')
        ], string='O que você deseja zerar?', default=_get_ids, required=True)
    dobra = fields.Boolean('É Dobra')
    terapeuta_dobra = fields.Many2one('sale.fila', string='Terapeuta')
    
    
    @api.multi
    def zera_fila(self):
        sale_fila = self.env['sale.fila'].browse(self._context.get('active_ids', []))
        hr_attatrasos = self.env['hr.attatrasos'].browse()
        user_currency_id = self.env.user.company_id.currency_id.id
        
        if self.selecao_zera == 0 or self.selecao_zera == 2:
            # Zera Apenas as Pontuações
            for line in sale_fila:
                line.vr_turno = 0
                line.ultima_zerada = fields.Datetime.now()
                
                if line.employee_id == self.terapeuta_dobra:
                    line.kanban_state = 1
                    
        
        if self.selecao_zera == 1 or self.selecao_zera == 2:
            # Zera Apenas os Atrasos
            for line in sale_fila:
                line.vr_atraso_ad = 0
                line.vr_atraso_ch = 0
                terapeuta_id = line.employee_id.id
                
            where_query = hr_attatrasos._where_calc([
                ('company_id', '=', self.env.user.company_id.id), ('massagista_id', '=', terapeuta_id)])
            hr_attatrasos._apply_ir_rules(where_query, 'read')
            from_clause, where_clause, where_clause_params = where_query.get_sql()
                
            query = """
                  UPDATE hr_attatrasos
                  SET status = 'inat'
                  WHERE %s
                    """ % where_clause
                
            self.env.cr.execute(query, where_clause_params)
            
             
class SaleMudaStatusT(models.TransientModel):
    _name = "sale.mudastatust"
    _description = "Muda Status Terapeuta"
    
    
    @api.model
    def _get_ids(self):
        salefila_obj = self.env['sale.fila']
        order = salefila_obj.browse(self._context.get('active_ids'))[0]
        return 0

    selecao_status = fields.Selection([
        (2, 'Livre'),
        (4, 'Ausente')
        ], string='Escolhe o Status que Deseja Atribuir ao Terapeuta: ', default=_get_ids, required=True)
    
    @api.multi
    def muda_status(self):
        sale_fila = self.env['sale.fila'].browse(self._context.get('active_ids', []))
        for line in sale_fila:
            line.kanban_state = self.selecao_status
        

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    sala_id = fields.Many2one('sale.salas', string='Sala')
    massagista_id = fields.Many2one('sale.fila', string='Terapeuta')
    preferencial = fields.Selection([
        ('n','Sem Preferência'),
        ('pt', 'Preferência Terapeuta'),
        ('ph', 'Preferência Homem'),
        ('pm', 'Preferência Mulher'),
        ('po', 'Preferência Oriental'),
        ('poh', 'Preferência Oriental Homem'),
        ('pom', 'Preferência Oriental Mulher')], "Preferêncial", default='n')

    e_pacote = fields.Boolean('Necessita de Terapeuta e Sala ou Cadeira', default=False, compute='_compute_e_pacote')
    vr_desconto = fields.Float('Valor do Desconto', digits=(6, 2), default=0.0, readonly=False)
    date_start = fields.Datetime(string="Data e Hora do Início")
    date_end = fields.Datetime(string="Data e Hora do Fim")
    divisao_item = fields.Selection([
        ('comum', 'Comum'),
        ('p','Pacote'),
        ('ip', 'Item Pacote'),
        ('comb', 'Combo'),
        ('vou', 'Voucher'),
        ('vale', 'Vale Presente')], "Divisão do Item", default='c')
    produto_id = fields.Many2one(
        comodel_name='sped.produto',
        string='Produto',
    )

    @api.multi
    def _compute_e_pacote(self):
        for order in self:
            if order.order_id.tipo_produto == "pacote" or order.order_id.tipo_produto == "add_vp":
                return {'readonly': {'massagista_id': True, 'sala_id': True}}
            elif order.order_id.tipo_produto == "pacote" or order.order_id.tipo_produto == "add_vp":
                return {'readonly': {'massagista_id': False, 'sala_id': False}}

    @api.multi
    @api.onchange('produto_id')
    def produto_id_change(self):
        record = super(SaleOrderLine, self)._onchange_produto_id()
        produto_sped = self.env['sped.produto'].search([('id','=',self.produto_id.id)])
        vals = {}
        
        if self.produto_id != False:
            for product in produto_sped:

                self.price_unit = product.preco_venda
                if product.product_id.item_pacote_ok == True:
                    self.vr_desconto = self.price_unit

        return record
     
    @api.multi
    @api.onchange('vr_unitario_readonly')
    def _change_domain(self):
        if self.produto_id.id > 0 and self.order_id.tipo_produto != 'pacote' and self.order_id.tipo_produto != 'add_vp':
            domain_t = []
            domain_s = []
            terapeutas_ids = []
            salas_ids = []
            ids_o = []
            ids_sso = []
            ids_f = []
            ids_t = []
            cal_ids = []
            
            product_time = self.env['product.template'].search([('id','=',self.produto_id.product_tmpl_id.id)])
            for product in product_time:
                min = product.uom_id.factor * 60
            
            start = datetime.strptime(self.order_id.data_ordem, DEFAULT_SERVER_DATETIME_FORMAT)
            end = datetime.strptime(self.order_id.data_ordem, DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(minutes=min)
            
            start_time = float(start.time().hour) + (float(start.time().minute) / 60)
            end_time = float(end.time().hour) + (float(end.time().minute) / 60)
            
            start_week = start.weekday()
            
            # pega terapeutas ocupados
            query_args = {'inicio': start, 'fim': end}
            query = """
                SELECT sf.massagista_id
                    FROM calendar_event ce
                    INNER JOIN sale_order_line sol ON ce.order_id = sol.order_id
                    INNER JOIN sale_fila sf ON sol.massagista_id = sf.id
                    WHERE (%(inicio)s BETWEEN ce.start AND ce.stop
                    OR %(fim)s BETWEEN ce.start AND ce.stop
                    OR ce.start BETWEEN %(inicio)s AND %(fim)s
                    OR ce.stop BETWEEN %(inicio)s AND %(fim)s)
                    ORDER BY ce.start"""
                
            self._cr.execute(query, query_args)
            ids_o = [item[0] for item in self.env.cr.fetchall()]
            
            # pega salas ocupadas
            query_args = {'inicio': start, 'fim': end}
            query = """
                SELECT ss.id
                    FROM calendar_event ce
                    INNER JOIN sale_order_line sol ON ce.order_id = sol.order_id
                    INNER JOIN sale_salas ss ON sol.sala_id = ss.id
                    WHERE (%(inicio)s BETWEEN ce.start AND ce.stop
                    OR %(fim)s BETWEEN ce.start AND ce.stop
                    OR ce.start BETWEEN %(inicio)s AND %(fim)s
                    OR ce.stop BETWEEN %(inicio)s AND %(fim)s)
                    ORDER BY ce.start"""
                
            self._cr.execute(query, query_args)
            ids_sso = [item[0] for item in self.env.cr.fetchall()]
            
            # pega terapeutas de folga
            query_args = {'inicio': start}
            query = """
                SELECT sp.id
                    FROM sped_participante sp
                    INNER JOIN clinica_folga cf ON cf.paticipante_id = sp.id
                    WHERE %(inicio)s BETWEEN hh.date_from AND hh.date_to
                    AND hh.holiday_type = 'employee'
                    GROUP BY 1"""
                
            self._cr.execute(query, query_args)
            ids_f = [item[0] for item in self.env.cr.fetchall()]
            
            # pega terapeutas que trabalham no dia
            tt_rca = self.env['resource.calendar.attendance'].search([('dayofweek','=',start_week),('hour_from','<=',start_time),('hour_to','>=',end_time)])
            for tt in tt_rca:
                date_agenda = datetime.strptime(self.order_id.date_order, DEFAULT_SERVER_DATETIME_FORMAT).date()
                if date_agenda == tt.date_from and date_agenda == tt.date_to:
                    if tt.calendar_id.id not in cal_ids:
                        cal_ids.append(tt.calendar_id.id)
                else:
                    if tt.calendar_id.id not in cal_ids and tt.date_from == False:
                        cal_ids.append(tt.calendar_id.id)
            
            tt_rr = self.env['resource.resource'].search([('calendar_id','=',cal_ids)],order='id')
            for tt in tt_rr:
                hr_hr = self.env['hr.employee'].search([('resource_id','=',tt.id)])
                for hr_tt in hr_hr:
                    if hr_tt.id not in ids_t:
                        ids_t.append(hr_tt.id)
            
            # Monta o Filtro Terapeuta
            product_select = self.env['hr.employee'].search([('product_ids','=',self.product_id.product_tmpl_id.id)])
            for prod in product_select:
                if prod.id > 0 and len(ids_o) == 0 and len(ids_f) == 0 and prod.id in ids_t:
                    terapeutas_ids.append(prod.id)
                elif prod.id > 0 and (prod.id not in ids_o or len(ids_o) == 0 ) and (prod.id not in ids_f or len(ids_f) == 0) and prod.id in ids_t:
                    terapeutas_ids.append(prod.id)
            
            # Monta o Filtro Sala
            salas_select = self.env['sale.salas'].search([('status','!=','manutencao')])
            for salas in salas_select:
                if len(ids_sso) == 0 or salas.id in ids_sso:
                    salas_ids.append(salas.id)
            
            domain_t = [('employee_id','in',terapeutas_ids)]
            domain_s = [('id','in',salas_ids)]
            if len(terapeutas_ids) != 0:
                if len(salas_ids) == 0:
                    return {'domain': {'massagista_id': str(domain_t)}}
                else:
                    return {'domain': {'massagista_id': str(domain_t), 'sala_id': str(domain_s)}}
            
            else:
                ids_ce = []
                ids_calen = []
                ids_resour = []
                ids_tp = []
                
                if len(ids_o) != 0:
                    
                    name_tera = ''
                    
                    # pega vendas
                    query_args = {'inicio': start, 'fim': end}
                    query = """
                        SELECT ce.id
                        FROM calendar_event ce
                        INNER JOIN sale_order_line sol ON ce.order_id = sol.order_id
                        INNER JOIN sale_fila sf ON sol.massagista_id = sf.id
                        WHERE (%(inicio)s BETWEEN ce.start AND ce.stop
                        OR %(fim)s BETWEEN ce.start AND ce.stop
                        OR ce.start BETWEEN %(inicio)s AND %(fim)s
                        OR ce.stop BETWEEN %(inicio)s AND %(fim)s)
                        ORDER BY ce.stop"""
                
                    self._cr.execute(query, query_args)
                    ids_ce = [item[0] for item in self.env.cr.fetchall()]

                    ce_select = self.env['calendar.event'].search([('order_id', '=', ids_ce[0])])
                    for date_hr in ce_select:
                        dthr_fim = date_hr.stop

                    horas_fim = float(dthr_fim.time().hour) + (float(dthr_fim.time().minute) / 60)                     
                    # pega próximos turnos 
                    dts_pro = self.env['resource.calendar.attendance'].search([('hour_from','<=',horas_fim)])
                    for x in dts_pro:
                        if horas_fim > x.hour_from:
                            ids_calen.append(x.calendar_id.id)
                    
                    tt_rr2 = self.env['resource.resource'].search([('calendar_id','=',ids_calen)])
                    for tt in tt_rr2:
                        hr_hr2 = self.env['hr.employee'].search([('resource_id','=',tt.id)])
                        for hr_tt in hr_hr2:
                            ids_tp.append(hr_tt.id)
                            
                    if len(ids_tp) != 0:
                        
                        # pega próximos turnos 
                        hr_emp = self.env['hr.employee'].search([('id','=',ids_tp)])
                        for w in hr_emp:
                            name_tera = name_tera + ', ' + str(w.name_related)
                        
                        hora_pro = self.env['resource.calendar.attendance'].search([('calendar_id','=',self.massagista_id.employee_id.resource_id.calendar_id.id)])
                        for h_p in hora_pro:
                            hora_proturno = h_p.date_from
                        
                        my_tz = pytz.timezone('America/Sao_Paulo')
                        my_dt = datetime.strptime(hora_proturno, DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(minutes=5)
                        utc_dt = my_dt.replace(tzinfo=pytz.timezone('UTC'))
                        dt_ajuste = utc_dt.astimezone(my_tz)
                
                        minha_dt = str(dt_ajuste.date().day) + '/' + str(dt_ajuste.date().month) + '/' + str(dt_ajuste.date().year)
                
                        if dt_ajuste.time().minute < 10:
                            meu_minuto = '0' + str(dt_ajuste.time().minute)
                        else:
                            meu_minuto = str(dt_ajuste.time().minute)
                    
                        minha_hr = str(dt_ajuste.time().hour) + ':' + meu_minuto
                        
                        raise UserError(_('Para o produto "%s", o próximo horário é no turno seguinte com os Terapeutas "%s" dia "%s" às "%s".') %
                                        (self.product_id.name, name_tera, minha_dt, minha_hr))
                    
                    sol_select = self.env['sale.order.line'].search([('order_id','=',ids_ce[0])])
                    for sol in sol_select:
                        sf_select = self.env['sale.fila'].search([('id','=',sol.massagista_id)])
                        for sf in sf_select:
                            hr_select = self.env['hr.employee'].search([('id','=',sf.employee_id)])
                            for name_hr in hr_select:
                                name_tera = str(name_hr.name_related)
                    
                    my_tz = pytz.timezone('America/Sao_Paulo')
                    my_dt = datetime.strptime(horas_fim, DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(minutes=5)
                    utc_dt = my_dt.replace(tzinfo=pytz.timezone('UTC'))
                    dt_ajuste = utc_dt.astimezone(my_tz)
                
                    minha_dt = str(dt_ajuste.date().day) + '/' + str(dt_ajuste.date().month) + '/' + str(dt_ajuste.date().year)
                
                    if dt_ajuste.time().minute < 10:
                         meu_minuto = '0' + str(dt_ajuste.time().minute)
                    else:
                        meu_minuto = str(dt_ajuste.time().minute)
                    
                    minha_hr = str(dt_ajuste.time().hour) + ':' + meu_minuto
                                
                    raise UserError(_('Para o produto "%s", o próximo horário é com Terapeuta "%s" dia "%s" às "%s".') %
                                    (self.product_id.name, name_tera, minha_dt, minha_hr))
                
                elif len(ids_f) != 0:
                    # pega terapeutas de folga
                    query_args = {'inicio': start}
                    query = """
                                    SELECT hre.name_related
                                        FROM hr_employee hre
                                        INNER JOIN hr_holidays hh ON hh.employee_id = hre.id
                                        WHERE %(inicio)s BETWEEN hh.date_from AND hh.date_to
                                        AND hh.holiday_type = 'employee'
                                        GROUP BY 1"""

                    self._cr.execute(query, query_args)
                    name_teras = [item[0] for item in self.env.cr.fetchall()]

                    if len(name_teras) == 1:

                        raise UserError(_('O Terapeuta "%s" estará de folga neste dia.') %
                                        (name_teras))

                    elif len(name_teras) > 1:

                        raise UserError(_('Os Terapeutas "%s" estaram de folga neste dia.') %
                                        (name_teras))
                    
                elif len(ids_t) == 0:
                    raise UserError(_('Nenhum terapeuta estará trabalhando, mude a data e/ou o horário.'))
                
                elif len(salas_ids) == 0 and self.sala == True:
                    
                    name_tera = ''
                    
                    # pega vendas
                    query_args = {'inicio': start, 'fim': end}
                    query = """
                        SELECT ce.id
                        FROM calendar_event ce
                        INNER JOIN sale_order_line sol ON ce.order_id = sol.order_id
                        INNER JOIN sale_salas ss ON sol.sala_id = ss.id
                        WHERE (%(inicio)s BETWEEN ce.start AND ce.stop
                        OR %(fim)s BETWEEN ce.start AND ce.stop
                        OR ce.start BETWEEN %(inicio)s AND %(fim)s
                        OR ce.stop BETWEEN %(inicio)s AND %(fim)s)
                        ORDER BY ce.stop"""
                
                    self._cr.execute(query, query_args)
                    ids_ce = [item[0] for item in self.env.cr.fetchall()]
                    
                    ce_select = self.env['calendar.event'].search([('order_id','=',ids_ce[0])])
                    for date_hr in ce_select:
                        dthr_fim = date_hr.stop
                        
                    sol_select = self.env['sale.order.line'].search([('order_id','=',ids_ce[0])])
                    for sol in sol_select:
                        ss_select = self.env['sale.salas'].search([('id','=',sol.sala_id)])
                        for ss in ss_select:
                            name_sala = str(ss.sala_name)
                            n_s = str(ss.numero_sala)
                            tipo_sc = str(ss.tipo)
                    
                    horas_fim = float(dthr_fim.time().hour) + (float(dthr_fim.time().minute) / 60)    
                    
                    my_tz = pytz.timezone('America/Sao_Paulo')
                    my_dt = datetime.strptime(horas_fim, DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(minutes=5)
                    utc_dt = my_dt.replace(tzinfo=pytz.timezone('UTC'))
                    dt_ajuste = utc_dt.astimezone(my_tz)
                
                    minha_dt = str(dt_ajuste.date().day) + '/' + str(dt_ajuste.date().month) + '/' + str(dt_ajuste.date().year)
                
                    if dt_ajuste.time().minute < 10:
                         meu_minuto = '0' + str(dt_ajuste.time().minute)
                    else:
                        meu_minuto = str(dt_ajuste.time().minute)
                    
                    minha_hr = str(dt_ajuste.time().hour) + ':' + meu_minuto
                    
                    raise UserError(_('Nesse horário não tem sala ou cadeira disponíveis, o próximo horário é na "%s" ("%s") Nº no dia "%s" às "%s".') %
                                    (name_sala, tipo_sc, n_s, minha_dt, minha_hr))

        elif self.order_id.tipo_produto == 'pacote':
            return {'readonly': {'massagista_id': True}}

        elif self.order_id.tipo_produto == 'add_vp':
            return {'readonly': {'massagista_id': True}}


    @api.onchange('vr_desconto')
    def vr_desconto_change(self):
        for desconto in self:
            if desconto.vr_desconto > 0:
                desconto.discount = (desconto.vr_desconto / desconto.price_unit) * 100
        
    @api.onchange('discount')
    def discount_change(self):
        for desconto in self:
            if desconto.discount > 0:
                desconto.vr_desconto = (desconto.discount / 100) * desconto.price_unit

    @api.multi
    def _create_calendar_event(self):
        """ This method will create entry in resource calendar event object """
        for event in self:
            product_time = self.env['product.template'].search([('id', '=', event.produto_id.product_tmpl_id.id)])
            for pro in product_time:
                if pro.pacote_ok == False:
                    event.date_start = event.order_id.date_end
                    event.date_end = datetime.strptime(event.date_start,
                                                       DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(hours=pro.duracao)
                    event.order_id.date_end = event.date_end

                    self.env['calendar.event'].create({
                        'name': 'AGENDAMENTO DE MASSAGEM',
                        'start': event.date_start,
                        'stop': event.date_end,
                        'allday': False,
                        'state': 'open',  # to block that meeting date in the calendar
                        'privacy': 'public',
                        'order_id': event.order_id.id,
                        'sala_id': event.sala_id.id,
                        'employee_id': event.massagista_id.employee_id.id,
                        'description': event.order_id.note,
                        'partner_ids': [(4, event.order_id.partner_id.id)],
                        'categ_ids': [(4, 1)],
                    })
                elif pro.pacote_ok == True:
                    break

        return True

    @api.multi
    def _create_saldo(self):
        for saldo in self:
            product_saldo = self.env['product.template'].search([('id', '=', saldo.produto_id.product_tmpl_id.id)])
            for pro_sal in product_saldo:
                if pro_sal.pacote_ok == True:
                    val_pac = datetime.strptime(saldo.order_id.date_order,
                                                       DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(days=pro_sal.prazo_validade)
                    self.env['sale.pacote'].create({
                        'partner_id': saldo.order_id.participante_id.id,
                        'produto': saldo.product_id.id,
                        'validade': val_pac,
                        'vr_pacote': saldo.price_total,
                        'vr_usado': 0,
                        'company_id': saldo.company_id.id,
                        'order_id': saldo.order_id.id,
                    })

    @api.multi
    def _baixa_saldo(self):
        for saldo in self:
            partner_saldo = self.env['sale.pacote'].search([('partner_id', '=', saldo.order_id.participante_id.id)])
            for par_sal in partner_saldo:
                if par_sal.saldo > saldo.price_total:
                    par_sal.vr_usado = saldo.price_total
                elif par_sal.saldo < saldo.price_total:
                    raise UserError(_(
                        'O cliente "%s" não possui saldo suficiente, o produto que deseja custa "%s" e o saldo do cliente é "%s".') %
                                    (saldo.order_id.participante_id.nome, saldo.price_total, par_sal.saldo))

    @api.model
    def create(self, values):
        # Override the original create function for the res.partner model
        record = super(SaleOrderLine, self).create(values)
        record._preenche_divisao()
        record._create_calendar_event()
        if self.order_id.tipo_produto == "pacote":
            record._create_saldo()
        if self.order_id.tipo_produto == "pacote":
            record._baixa_saldo()
        return record

class SaleUsoPacotes(models.Model):
    _name = 'sale.usopacotes'
    _description = 'Uso dos Pacotes'


    product_id = fields.Many2one('product.product', string='Produto Que Usou', ondelete='restrict')
    valor_produto = fields.Float('Valor do Produto Usado')
    data_hora_uso = fields.Datetime('Data e Hora do Uso')
    order_id = fields.Many2one('sale.order', string='Order Reference', required=True, ondelete='cascade', index=True,
                               copy=False)
    calendar_event_id = fields.Many2one('calendar.event', string='Calendário Referente', ondelete='cascade')
    date_start = fields.Datetime(string="Data e Hora do Início")
    date_end = fields.Datetime(string="Data e Hora do Fim")


    @api.onchange('product_id')
    def _product_change(self):
        for line in self:
            line.valor_produto = line.product_id.sped_produto_id.preco_venda

    @api.multi
    def _preenche_campos(self):
        for line in self:
            line.data_hora_uso = fields.Datetime.now()

    @api.model
    def create(self, values):
        # Override the original create function for the res.partner model
        record = super(SaleUsoPacotes, self).create(values)
        record._cria_calendar_event()
        record._preenche_campos()
        return record