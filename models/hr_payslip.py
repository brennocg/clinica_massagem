# -*- coding: utf-8 -*-
from odoo import api, fields, models


class HrPayslipLine(models.Model):
    _inherit = 'hr.payslip.line'
    
    @api.multi
    @api.depends('quantity', 'amount', 'rate')
    def _compute_total(self):
        sale_fila = self.env['sale.fila']
        for line in self:
            if line.code == 'comissao':
                
                result = 0.0
                self.env.cr.execute("""
                          SELECT SUM(so.valor_massagista) as total, so.massagista_id
                            FROM hr_employee hre
                            Left Join sale_fila sf ON sf.employee_id = hre.id
                            Left Join sale_order so ON so.massagista_id = sf.id
                           WHERE hre.id = %s
                               AND so.company_id=%s
                               AND (so.date_order between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))
                           GROUP BY so.massagista_id""",
                (line.slip_id.employee_id.id, self.env.user.company_id.id, line.slip_id.date_from, line.slip_id.date_to,))
                result = self.env.cr.fetchone()[0] or 0.0
                line.total = float(result or 0.0)
                
                
                result = 0.0
                self.env.cr.execute("""
                          SELECT SUM(so.valor_massagista_qbr) as total, so.massagista_id_quebra
                            FROM hr_employee hre
                            Left Join sale_fila sf ON sf.employee_id = hre.id
                            Left Join sale_order so ON so.massagista_id_quebra = sf.id
                           WHERE hre.id = %s
                               AND so.company_id=%s
                               AND (so.date_order between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))
                           GROUP BY so.massagista_id_quebra""",
                (line.slip_id.employee_id.id, self.env.user.company_id.id, line.slip_id.date_from, line.slip_id.date_to,))
                result = self.env.cr.fetchone()
                if result != None:
                    line.total += float(result or 0.0)
                
            else:
                
                line.total = float(line.quantity) * line.amount * line.rate / 100
               