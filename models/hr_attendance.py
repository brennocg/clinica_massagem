# -*- coding: utf-8 -*-

from odoo import models, fields, api, _    


class HrAttendance(models.Model):
    _inherit = 'hr.attendance'
    
    # Atraso
    check_in = fields.Datetime(string="Check In", default="", required=False)
    atraso = fields.Boolean('Atrasado')
    vr_atraso_ch = fields.Float('Atraso Chegada', digits=(6, 2), default=0.0)
    inativo = fields.Boolean('Inativo na Fila')
    bonus_atraso = fields.Boolean('Usar Bônus')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    
    
class HrAttAtrasos(models.Model):
    _name = 'hr.attatrasos'
    _description = 'Cadastro de Atrasos'
    _inherit = ['mail.thread']
    
    # Cadastro de Atrasos
    massagista_id = fields.Many2one('hr.employee', string='Massagista', domain=[('massagista', '=', True)])
    tipo = fields.Selection([
        ('ch', 'Chegada'),
        ('ad', 'Adicional')
    ], string='Tipo', default='ch', readonly=False)
    valor = fields.Float('Valor', required=True, digits=(6, 2), default=0.0)
    note = fields.Text('Motivo')
    status = fields.Selection([
        ('at', 'Ativo'),
        ('inat', 'Inativo')
    ], string='Status', default='at', readonly=False)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company']._company_default_get('hr.attatrasos'))
    
    @api.multi
    def name_get(self):
        result = []
        for atraso in self:
            result.append((atraso.id, _("%(terapeuta)s em %(data)s ") % {
                'terapeuta': atraso.massagista_id.name_related,
                'data': atraso.create_date,
            }))
        return result
    
    
        