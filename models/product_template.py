# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    duracao = fields.Float(string="Duração", digits=(2, 2), default=0.0)
    sala = fields.Boolean('Necessita de Sala/Cadeira', default=False)
    prazo_validade = fields.Integer('Prazo de Validade (dias)', default=90)
    comissao_prod = fields.Float('Comissão do Produto', digits=(2, 2), default=0.0)
    pacote_ok = fields.Boolean('É Pacote', default=False)
    item_pacote_ok = fields.Boolean('É Item de Pacote', default=False)
    combo_ok = fields.Boolean('É Combo', default=False)
    voucher_ok = fields.Boolean('É Voucher', default=False)
    valepresente_ok = fields.Boolean('É Vale Presente', default=False)
