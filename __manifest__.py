# -*- coding: utf-8 -*-
{
    'name': "Clínica de Massagem",

    'summary': """
        Aplicativo para Clínica de Massagem""",

    'description': """

        Índice:
    
    - Cadastro de Terapeutas.
    
    - Cadastro de Salas.
    
    - Cadastro de Combos, Pacotes e Lote de Vouchers
    
    - Fila de Atendimento.
    
    - Controle de Atividades.
    
    - Controle de Cortesias.
    
    - Gestão de Turnos.
	
    """,

    'author': "Recta.dos, Brenno",
    'website': "http://www.rectados.com.br",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'hr',
        'sale',
        'hr_attendance',
        'hr_holidays',
        'account',
        'calendar',
        'l10n_br_base',
        'sped_sale'
    ],

    # always loaded
    'data': [
        'views/hr_views.xml',
        'views/hr_attendance_view.xml',
        'views/sped_produto_base_view.xml',
        'views/inherited_sale_order_line_base_view.xml',
        'views/inherited_sale_order_orcamento_view.xml',
        'views/sale_views.xml',
        'views/sped_participante_base_view.xml',
        'views/hr_holidays_views.xml',
        'views/calendar_views.xml',
        'views/inherited_sale_order_line_usopacotes_view.xml',
        'security/ir.model.access.csv'
    ],

    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
