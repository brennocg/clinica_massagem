# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class SaleReport(models.Model):
    _inherit = 'sale.report'
    
    sala_id = fields.Many2one('sale.salas', string='Sala')
    massagista_id = fields.Many2one('sale.fila', string='Terapeuta')
    massagista_id_quebra = fields.Many2one('hr.employee', 'Terapeuta da Quebra', readonly=True)
    massagista_id_passagem = fields.Many2one('hr.employee', 'Terapeuta da Passagem', readonly=True)
    perc_comissao = fields.Float('Percentual Terapeuta', readonly=True)
    valor_massagista = fields.Float('Valor Terapeuta', readonly=True)
    tipo_produto = fields.Selection([
        ("comum_pacote", "Padrão"),
        ("cortesia", "Cortesia"),
        ("add_vp", "Adquirir Vale Presente"),
        ("add_lv", "Adquirir Lote Voucher"),
        ("usa_vp", "Usar Vale Presente"),
        ("usa_vo", "Usar Voucher")
        ], "Tipo do Produto", readonly=True)
    cortesia_prod = fields.Many2one('sale.cortesia', 'Cortesia', readonly=True)
    valevoucher_prod = fields.Many2one('sale.valevoucher', 'Vale PVoucher', readonly=True)
    vr_inicial = fields.Float('Número:', readonly=True)
    vr_final = fields.Float('Número do Último Vale ou Voucher:', readonly=True)
    preferencial = fields.Selection([
        ('n','Sem Preferência'),
        ('pt', 'Preferência Terapeuta'),
        ('ph', 'Preferência Homem'),
        ('pm', 'Preferência Mulher'),
        ('po', 'Preferência Oriental'),
        ('poh', 'Preferência Oriental Homem'),
        ('pom', 'Preferência Oriental Mulher')], "Preferêncial")
    quebra_massagem = fields.Boolean('Quebrar Massagem', readonly=True)
    passa_massagem = fields.Boolean('Passar Massagem', readonly=True)
    gestante = fields.Boolean('É Gestante', readonly=True)
    progress = fields.Selection([
            ('agendado', 'Agendado'),
            ('atendendo', 'Atendendo'),
            ('concluido', 'Concluído')
        ],'Status do Atendimento', readonly=True)
    
    def _select(self):
        return super(SaleReport, self)._select() + ", l.sala_id as sala_id" + ", l.massagista_id as massagista_id" + ", l.preferencial as preferencial" + ", s.massagista_id_quebra as massagista_id_quebra" + ", s.massagista_id_passagem as massagista_id_passagem" + ", s.perc_comissao as perc_comissao" + ", s.valor_massagista as valor_massagista" + ", s.tipo_produto as tipo_produto" + ", s.cortesia_prod as cortesia_prod" + ", s.valevoucher_prod as valevoucher_prod" + ", s.vr_inicial as vr_inicial" + ", s.vr_final as vr_final" + ", s.quebra_massagem as quebra_massagem" + ", s.gestante as gestante" + ", s.progress as progress" 
    
    def _group_by(self):
        return super(SaleReport, self)._group_by() + ", l.sala_id" + ", l.massagista_id" + ", l.preferencial" + ", s.massagista_id_quebra" + ", s.massagista_id_passagem" + ", s.perc_comissao" + ", s.valor_massagista" + ", s.tipo_produto" + ", s.cortesia_prod" + ", s.valevoucher_prod" + ", s.vr_inicial" + ", s.vr_final" + ", s.quebra_massagem" + ", s.gestante" + ", s.progress"
    
    #def _from(self):
    #    return super(SaleReport, self)._from() + "left join sale_salas ss on (l.sala_id=ss.id)" + "left join sale_fila sf on (l.massagista_id = sf.id)"
                                                    